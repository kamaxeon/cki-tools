"""Test cki.deployment_tools.render module."""
import itertools
import pathlib
import tempfile
import unittest
from unittest import mock

import responses

from cki.deployment_tools import render


class TestJinja2(unittest.TestCase):
    """Test jinja2 rendering."""

    def _test(self, template, output, *, data_files=None, data=None,
              raw_data=None, include_paths=None):
        # pylint: disable=too-many-arguments
        with tempfile.TemporaryDirectory() as temp_directory:
            temp_dir = pathlib.Path(temp_directory)

            template_file = temp_dir / 'template.txt'
            template_file.write_text(template)

            output_file = temp_dir / 'output.txt'

            for name, contents in (data_files or {}).items():
                data_file: pathlib.Path = temp_dir / name
                data_file.parent.mkdir(parents=True, exist_ok=True)
                data_file.write_text(contents)

            args = list(itertools.chain(
                [template_file.as_posix(), '--output', output_file.as_posix()],
                *(['--data', f'{k}={temp_dir / v}'] for k, v in (data or {}).items()),
                *(['--raw-data', f'{k}={temp_dir / v}'] for k, v in (raw_data or {}).items()),
                *(['--include-path', f'{temp_dir / p}'] for p in (include_paths or []))))

            if output:
                render.main(args)
                self.assertEqual(output_file.read_text(), output)
            else:
                self.assertRaises(Exception, render.main, args)

    @mock.patch.dict('cki.deployment_tools.render.os.environ', {'FOO': 'BAR'})
    def test_filter_env(self):
        """Test filter_env filter."""
        cases = [
            ('$FOO', 'BAR\n'),
            ('${FOO}', 'BAR\n'),
            ('${FOO', '${FOO\n'),
            ('$FOO}', 'BAR}\n'),
            ('{FOO}', '{FOO}\n'),
            ('#FOO', '#FOO\n'),
            ('$NOT_DEFINED_VAR', None),
        ]

        for variable, value in cases:
            self._test('{{ "' + variable + '" | env }}\n', value)

    @responses.activate
    def test_global_url(self):
        """Test global_url function."""
        responses.add(responses.GET, 'https://example.url/dummy', body='content')
        self._test('{{ url("https://example.url/dummy") }}\n', 'content\n')
        self._test('{{ url("https://example.url/dummy", binary=true) }}\n', "b'content'\n")

    @responses.activate
    def test_gloabal_url_json(self):
        """Test global_url function with json content."""
        responses.add(responses.GET, 'https://example.url/dummy', json={'foo': 'bar'})
        self._test('{{ url("https://example.url/dummy") }}\n', '{"foo": "bar"}\n')
        self._test('{{ url("https://example.url/dummy", json=true) }}\n', "{'foo': 'bar'}\n")

    @mock.patch.dict('os.environ', {'FOOBAR': 'BARBAR'})
    def test_render_nodata(self):
        """Test render function without a data file."""
        self._test('foo\n{{ env["FOOBAR"] }}\n', 'foo\nBARBAR\n')

    def test_render_noenv(self):
        """Test non-existing env variables raise an exception."""
        self._test('foo\n{{ env["FOOBAR"] }}\n', None)

    @mock.patch.dict('os.environ', {'FOOBAR': 'BARBAR'})
    def test_render(self):
        """Test render function."""
        self._test(
            'foo\n'
            '{{ data.foo }}\n'
            '{{ data.bar }}\n'
            '{{ data.bar|env }}\n'
            '{{ env["FOOBAR"] }}\n',
            'foo\n'
            'bar\n'
            '$FOOBAR\n'
            'BARBAR\n'
            'BARBAR\n',
            data_files={'data.yml': 'foo: bar\nbar: $FOOBAR'},
            data={'data': 'data.yml'},
        )

    def test_main_data_directory(self):
        """Test data directories."""
        self._test(
            '{{ data1.foo }}\n'
            '{{ data2.file1.foo }}\n'
            '{{ data2.dir1.file2.foo }}\n'
            '{{ data2.dir1.file3.foo }}\n'
            '{{ data2.dir1.file4.foo }}\n'
            '{{ data3["file1.raw"] }}\n'
            '{{ data3.dir2["file2.yml"] }}\n'
            '{{ data3.dir2["file3.yaml"] }}\n'
            '{{ data3.dir2["file4.json"] }}\n'
            '{{ data3.dir2["file5.json"] }}\n'
            '{{ data4 | tojson }}\n',
            'bar1\n'
            'bar2\nbar3\nbar4\nbar5\n'
            'raw-data-1\n"bar3"\n"bar4"\n"bar5"\n"bar6"\n'
            '{}\n',
            data_files={
                'data-file.yml': 'foo: bar1',
                'data-dir/file1.yml': '{ "foo": "bar2" }',
                'data-dir/dir1/file2.yml': '{ "foo": "bar3" }',
                'data-dir/dir1/file3.yaml': '{ "foo": "bar4" }',
                'data-dir/dir1/file4.json': '{ "foo": "bar5" }',
                'data-dir/dir1/file5.raw': '{ invalid json/yaml',
                'data-dir-2/file1.raw': 'raw-data-1',
                'data-dir-2/dir2/file2.yml': '"bar3"',
                'data-dir-2/dir2/file3.yaml': '"bar4"',
                'data-dir-2/dir2/file4.json': '"bar5"',
                'data-dir-2/dir2/file5.json.j2': '"{{ "bar6" }}"',
            },
            data={
                'data1': 'data-file.yml',
                'data2': 'data-dir',
            },
            raw_data={
                'data3': 'data-dir-2',
                'data4': 'non-existent/',
            },
        )

    def test_main_data_templates(self):
        """Test data file templating."""
        self._test(
            '{{ data1.foo }}\n'
            '{{ data2.file1.foo }}\n'
            '{{ data3 }}\n',
            'bar\n'
            'baz\n'
            'foo: bar\n',
            data_files={
                'data-file.yml.j2': 'foo: {{ "bar" }}',
                'data-dir/file1.json.j2': '{ "foo": "{{ "baz" }}" }',
            },
            data={
                'data1': 'data-file.yml.j2',
                'data2': 'data-dir',
            },
            raw_data={
                'data3': 'data-file.yml.j2',
            },
        )

    def test_main_includes(self):
        """Test jinja2 template including."""
        self._test(
            '{% include "data1.txt" %}\n',
            'foo\n',
            data_files={
                'data1.txt': '{{ "foo" }}',
            },
            include_paths=['.'],
        )

    def test_data_references_in_raw(self):
        """Test variable persistence across data files."""
        self._test(
            '{{ data2 }}\n',
            'bar\n',
            data_files={
                'data-file.yml.j2': 'foo: bar',
                'raw-data-file.txt.j2': '{{ data1.foo }}',
            },
            data={
                'data1': 'data-file.yml.j2',
            },
            raw_data={
                'data2': 'raw-data-file.txt.j2',
            },
        )
