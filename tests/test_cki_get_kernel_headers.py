"""Tests for cki/cki_tools/get_kernel_headers.py."""
import unittest
from unittest import mock

from cki.cki_tools import get_kernel_headers

HEADER_OUTPUT = """
Build                                       Built by       State
-----------------------------------         -------------  -----------
kernel-headers-5.8.0-0.fc33                 user1          COMPLETE
kernel-headers-5.8.0-1.fc33                 user2          COMPLETE
kernel-headers-5.8.0-0.rc7.git0.0.eln       user1          COMPLETE
kernel-headers-5.8.0-0.rc7.git0.1.eln103    user1          COMPLETE
kernel-headers-5.8.0-0.rc7.git0.0.eln103    user2          COMPLETE
"""


@mock.patch('cki.cki_tools.get_kernel_headers.KojiWrap.list_builds',
            mock.Mock(return_value=HEADER_OUTPUT))
class TestHeaderMatching(unittest.TestCase):
    """Test header correlation via NVRs."""

    koji = get_kernel_headers.KojiWrap('weburl', 'serverurl', 'topurl')

    def test_no_match(self):
        """Verify nothing gets returned if there is no match."""
        nvr = 'kernel-5.8.0-1.1.fc32'
        self.assertEqual(self.koji.koji_correlate_headers(nvr), [])

    def test_fedora(self):
        """Verify Fedora matching works."""
        nvr = 'kernel-5.8.0-1.fc33'
        self.assertEqual(
            len(self.koji.koji_correlate_headers(nvr, exact=False)),
            2
        )
        self.assertEqual(self.koji.koji_correlate_headers(nvr),
                         ['kernel-headers-5.8.0-1.fc33'])

    def test_eln(self):
        """Verify ELN matching works.

        Test both regular release and development buildroot (with a number)"""
        nvr_short = 'kernel-5.8.0-1.rc7.1.eln'
        self.assertEqual(
            self.koji.koji_correlate_headers(nvr_short, exact=False),
            ['kernel-headers-5.8.0-0.rc7.git0.0.eln']
        )

        nvr_long = 'kernel-5.8.0-0.rc7.1.eln103'
        self.assertEqual(self.koji.koji_correlate_headers(nvr_long), [])
        self.assertCountEqual(
            self.koji.koji_correlate_headers(nvr_long, exact=False),
            ['kernel-headers-5.8.0-0.rc7.git0.1.eln103',
             'kernel-headers-5.8.0-0.rc7.git0.0.eln103']
        )
