"""Check GitLab repos for the recommended configuration."""

import argparse
import datetime
import functools
import os
import pathlib
import re
import sys
from urllib import parse

from cki_lib import config_tree
from cki_lib import gitlab
from cki_lib import logger
import dateutil
from gitlab import exceptions as gl_exceptions
import yaml

LOGGER = logger.get_logger('cki.deployment_tools.gitlab_repo_config')


class RepoConfig:
    """Check GitLab repos for the recommended configuration."""

    def __init__(self, fix=False):
        """Create a new checker."""
        self._fix = fix

    @staticmethod
    @functools.lru_cache
    def _instance(instance_url):
        return gitlab.get_instance(instance_url)

    @functools.lru_cache
    def _group(self, instance_url, group):
        return self._instance(instance_url).groups.get(group)

    @functools.lru_cache
    def _project(self, project_url):
        url_parts = parse.urlsplit(project_url)
        instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
        project_path = url_parts.path[1:]
        return self._instance(instance_url).projects.get(project_path)

    def discover_group_projects(self, url, group):
        """Return a set of all projects in the given group."""
        return {p.web_url for p in self._group(url, group).projects.list(as_list=False)
                if not p.archived}

    def discover_config_projects(self, configs, only_project_url=None):
        """Return a dict of project name -> config for all projects mentioned in the config."""
        projects = {}
        for name, config in configs.items():
            if name.endswith('/default'):
                if only_project_url:
                    if only_project_url.startswith(f'{config["url"]}/{config["group"]}'):
                        projects.setdefault(only_project_url, config)
                else:
                    for url in self.discover_group_projects(config['url'], config['group']):
                        projects.setdefault(url, config)
            else:
                project_url = f'{config["url"]}/{config["group"]}/{name.split("/")[-1]}'
                if not only_project_url or only_project_url == project_url:
                    projects[project_url] = config
        LOGGER.debug('Discovered: %s', projects.keys())
        return projects

    def check_fork_ci(self, gl_project):
        """Check that forks have CI disabled."""
        for fork in gl_project.forks.list(as_list=False, owned=True):
            gl_fork = gl_project.manager.gitlab.projects.get(fork.id)
            if gl_fork.jobs_enabled:
                print(f'  unexpected enabled CI in fork {gl_fork.path_with_namespace}')
                if self._fix:
                    print('  fixing')
                    gl_fork.jobs_enabled = False
                    gl_fork.save()

    def check_project_creation(self, gl_group, config):
        """Check that nobody can create new projects."""
        if gl_group.project_creation_level != config['project_creation_level']:
            print(f'  unexpected project creation level {gl_group.project_creation_level}')
            if self._fix:
                print('  fixing')
                gl_group.project_creation_level = config['project_creation_level']
                gl_group.save()

    @staticmethod
    def check_branches(gl_project, config):
        """Check that there are no branches but the default branch."""
        branches = set(b.name for b in gl_project.branches.list(as_list=False))
        branches.remove(gl_project.default_branch)
        if branches != set(config['branches']):
            print(f'  unexpected branches {branches}')

    def check_project_field(self, gl_project, field_name, expected):
        """Check that private builds are configured correctly."""
        LOGGER.debug('Checking %s for %s', gl_project.path, field_name)
        if field_name not in gl_project.attributes:
            LOGGER.warning('Field %s not found', field_name)
            return
        if gl_project.attributes[field_name] != expected:
            print(f'  unexpected {field_name} != {expected}')
            if self._fix:
                print('  fixing')
                setattr(gl_project, field_name, expected)
                gl_project.save()

    def check_protectedbranches(self, gl_project, config):
        """Check that branch protection is configured correctly."""
        protectedbranches = [{
            'name': pb.name,
            'code_owner_approval_required': pb.attributes.get('code_owner_approval_required'),
            'push_access_level': pb.push_access_levels[0]['access_level'],
            'merge_access_level':pb.merge_access_levels[0]['access_level'],
        } for pb in gl_project.protectedbranches.list(as_list=False)]
        expected_protectedbranches = [{
            'name': name,
            **branch_config
        } for name, branch_config in config['protectedbranches'].items()]
        if protectedbranches != expected_protectedbranches:
            print(f'  unexpected branch protections {protectedbranches}')
            if self._fix:
                print('  fixing')
                for protectedbranch in gl_project.protectedbranches.list(as_list=False):
                    protectedbranch.delete()
                for expected_protectedbranch in expected_protectedbranches:
                    gl_project.protectedbranches.create(expected_protectedbranch)

    def check_codeowner(self, gl_project):
        """Check that codeowners and approvals are configured correctly."""
        try:
            gl_project.files.get('CODEOWNERS', ref='main')
        except gl_exceptions.GitlabGetError:
            return
        expected = {
            'approvals_before_merge': 1,
            'reset_approvals_on_push': True,
            'disable_overriding_approvers_per_merge_request': False,
            'merge_requests_author_approval': False,
            'merge_requests_disable_committers_approval': False,
            'require_password_to_approve': False,
        }

        gl_approvals = gl_project.approvals.get()
        if {k: v for k, v in gl_approvals.attributes.items() if k in expected.keys()} != expected:
            print('  unexpected approval settings')
            if self._fix:
                print('  fixing')
                gl_project.approvals.update(**expected)

        for gl_rule in gl_project.approvalrules.list(as_list=False):
            if gl_rule.rule_type == 'any_approver':
                if gl_rule.approvals_required > 0:
                    print('  too many approvers required')
                    if self._fix:
                        print('  fixing')
                        gl_rule.approvals_required = 0
                        gl_rule.save()
            else:
                print('  superfluous approval rule')
                if self._fix:
                    print('  fixing')
                    gl_rule.delete()

    def check_environments(self, gl_project, config):
        """Check that no stale environments exist."""
        if gl_project.operations_access_level == 'disabled':
            return
        expired = []
        for gl_environment in gl_project.environments.list(as_list=False):
            if any(re.fullmatch(e, gl_environment.name) for e in config['environments']['keep']):
                continue
            gl_environment = gl_project.environments.get(gl_environment.id)
            if gl_environment.last_deployment:
                updated_at = dateutil.parser.parse(gl_environment.last_deployment['updated_at'])
                if (updated_at + datetime.timedelta(days=config['environments']['expire_days'])
                        > datetime.datetime.utcnow().astimezone(datetime.timezone.utc)):
                    continue
            print(f'  expired environment {gl_environment.name}')
            expired.append(gl_environment)
        if self._fix and expired:
            print('  fixing')
            for gl_environment in expired:
                if gl_environment.state != 'stopped':
                    gl_environment.stop()
                gl_environment.delete()

    def run(self, configs, only_project_url=None):
        """Run all checkers."""
        print('Discovering projects')
        projects = self.discover_config_projects(configs, only_project_url)
        for project_url, config in projects.items():
            gl_project = self._project(project_url)
            if config['ignore']:
                LOGGER.debug('Ignoring %s', project_url)
                continue

            print(f'Processing {project_url}')
            gl_group = self._group(gl_project.manager.gitlab.url, gl_project.namespace['full_path'])
            self.check_project_creation(gl_group, config)
            self.check_protectedbranches(gl_project, config)
            self.check_branches(gl_project, config)
            self.check_fork_ci(gl_project)
            self.check_codeowner(gl_project)
            self.check_environments(gl_project, config)
            for field, expected in config['project'].items():
                self.check_project_field(gl_project, field, expected)


def main(args):
    """Run the main CLI interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--fix', action='store_true',
                        help='Fix detected problems')
    parser.add_argument('--project-url',
                        help='Only run the checker for one project')
    parser.add_argument('--config-path',
                        default=os.environ.get('GITLAB_REPO_CONFIG_PATH', 'config.yml'),
                        help='Path to the config file')
    parsed_args = parser.parse_args(args)

    configs = config_tree.process_config_tree(yaml.safe_load(
        os.environ.get('GITLAB_REPO_CONFIG') or
        pathlib.Path(parsed_args.config_path).read_text()))

    RepoConfig(fix=parsed_args.fix).run(configs, only_project_url=parsed_args.project_url)


if __name__ == '__main__':
    main(sys.argv[1:])
